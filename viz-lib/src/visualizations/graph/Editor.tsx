import { map, merge } from "lodash";
import React from "react";
import * as Grid from "antd/lib/grid";
import { Section, Select, InputNumber, ControlLabel } from "@/components/visualizations/editor";
import { EditorPropTypes } from "@/visualizations/prop-types";

export default function Editor({ options, data, onOptionsChange }: any) {
  const optionsChanged = (newOptions: any) => {
    onOptionsChange(merge({}, options, newOptions));
  };

  return (
    <React.Fragment>
      {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
      <Section>
        <Select
          label="Initial Column"
          data-test="Graph.InitialColumn"
          value={options.initialColumn}
          onChange={(initialColumn: any) => optionsChanged({ initialColumn })}>
          {map(data.columns, ({ name }) => (
            // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
            <Select.Option key={name} data-test={"Graph.InitialColumn." + name}>
              {name}
              {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
            </Select.Option>
          ))}
        </Select>
      </Section>
        {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
        <Section>
            <Select
                label="Class of Initial Column"
                data-test="Graph.ClassInitialColumn"
                value={options.classInitialColumn}
                onChange={(classInitialColumn: any) => optionsChanged({ classInitialColumn })}>
                {map(data.columns, ({ name }) => (
                    // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
                    <Select.Option key={name} data-test={"Graph.ClassInitialColumn." + name}>
                        {name}
                        {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
                    </Select.Option>
                ))}
            </Select>
        </Section>
        {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
        <Section>
            <Select
                label="Image of Initial Column"
                data-test="Graph.ImageInitialColumn"
                value={options.imageInitialColumn}
                onChange={(imageInitialColumn: any) => optionsChanged({ imageInitialColumn })}>
                {map(data.columns, ({ name }) => (
                    // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
                    <Select.Option key={name} data-test={"Graph.ImageInitialColumn." + name}>
                        {name}
                        {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
                    </Select.Option>
                ))}
            </Select>
        </Section>
      {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
      <Section>
        <Select
          label="Final Column"
          data-test="Graph.FinalColumn"
          value={options.finalColumn}
          onChange={(finalColumn: any) => optionsChanged({ finalColumn })}>
          {map(data.columns, ({ name }) => (
            // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
            <Select.Option key={name} data-test={"Graph.FinalColumn." + name}>
              {name}
              {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
            </Select.Option>
          ))}
        </Select>
      </Section>
        {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
        <Section>
            <Select
                label="Class of Final Column"
                data-test="Graph.ClassFinalColumn"
                value={options.classFinalColumn}
                onChange={(classFinalColumn: any) => optionsChanged({ classFinalColumn })}>
                {map(data.columns, ({ name }) => (
                    // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
                    <Select.Option key={name} data-test={"Graph.ClassFinalColumn." + name}>
                        {name}
                        {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
                    </Select.Option>
                ))}
            </Select>
        </Section>
        {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
        <Section>
            <Select
                label="Image of Final Column"
                data-test="Graph.ImageFinalColumn"
                value={options.imageFinalColumn}
                onChange={(imageFinalColumn: any) => optionsChanged({ imageFinalColumn })}>
                {map(data.columns, ({ name }) => (
                    // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
                    <Select.Option key={name} data-test={"Graph.ImageFinalColumn." + name}>
                        {name}
                        {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
                    </Select.Option>
                ))}
            </Select>
        </Section>
      {/* @ts-expect-error ts-migrate(2745) FIXME: This JSX tag's 'children' prop expects type 'never... Remove this comment to see the full error message */}
      <Section>
        <Select
          label="Relationship Column"
          data-test="Graph.RelationshipColumn"
          value={options.relationshipColumn}
          onChange={(relationshipColumn: any) => optionsChanged({ relationshipColumn })}>
          {map(data.columns, ({ name }) => (
            // @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message
            <Select.Option key={name} data-test={"Graph.RelationshipColumn." + name}>
              {name}
              {/* @ts-expect-error ts-migrate(2339) FIXME: Property 'Option' does not exist on type '({ class... Remove this comment to see the full error message */}
            </Select.Option>
          ))}
        </Select>
      </Section>
    </React.Fragment>
  );
}

Editor.propTypes = EditorPropTypes;
