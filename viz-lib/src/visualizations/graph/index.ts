import { merge } from "lodash";

import Renderer from "./Renderer";
import Editor from "./Editor";

const DEFAULT_OPTIONS = {
  initialColumn: "",
  finalColumn: "",
  relationshipColumn: ""
};

export default {
  type: "GRAPH",
  name: "Graph",
  getOptions: (options: any) => merge({}, DEFAULT_OPTIONS, options),
  Renderer,
  Editor,

  defaultRows: 8,
};
