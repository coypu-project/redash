import d3 from "d3";
import cloud from "d3-cloud";
import { each, filter, map, min, max, sortBy, toString } from "lodash";
import React, { useMemo, useState, useEffect } from "react";
import resizeObserver from "@/services/resizeObserver";
import { RendererPropTypes } from "@/visualizations/prop-types";
import ReactFlow, {
    removeElements,
    addEdge,
    isNode,
    MiniMap,
    Controls,
    Background, Handle, Position,
} from 'react-flow-renderer';

const dagre = require('dagre');
const dagreGraph = new dagre.graphlib.Graph();
dagreGraph.setDefaultEdgeLabel(() => ({}));

const defaultElements = [
  { id: 'e1-2', source: '1', target: '2', label: 'relationship', arrowHeadType: 'arrowclosed' },
  { id: '1', data: { label: 'Initial Node' }, position: { x: 250, y: 5 } },
  { id: '2', data: { label: 'Final Node' }, position: { x: 100, y: 100 } },
];

const nodeWidth = 172;
const nodeHeight = 36;
const defaultXPosition = 250;
const defaulYPosition = 5;

const getLayoutedElements = (elements : any, direction = 'LR') => {
    const isHorizontal = direction === 'LR';
    dagreGraph.setGraph({ rankdir: direction });

    elements.forEach((el : any) => {
        if (isNode(el)) {
            dagreGraph.setNode(el.id, { width: nodeWidth, height: nodeHeight });
        } else {
            dagreGraph.setEdge(el.source, el.target);
        }
    });

    dagre.layout(dagreGraph);

    return elements.map((el : any) => {
        if (isNode(el)) {
            const nodeWithPosition = dagreGraph.node(el.id);
            // @ts-ignore
            el.targetPosition = isHorizontal ? 'left' : 'top';
            // @ts-ignore
            el.sourcePosition = isHorizontal ? 'right' : 'bottom';

            // This hack must be done in order to notify react flow about the change. React flow expects that in
            // future versions it won't be necessary
            el.position = {
                x: nodeWithPosition.x - nodeWidth / 2 + Math.random() / 1000,
                y: nodeWithPosition.y - nodeHeight / 2,
            };
        }

        return el;
    });
};

function getNodeGraph (rows: any, options: any) {
    let nodes: string[] = [];
    let elements: any = [];

    if (rows.length === 0)
        return defaultElements;

    each(rows, row => {
        const startingNode = toString(row[options.initialColumn]);
        const classStartingNode = toString(row[options.classInitialColumn]);
        const imageStartingNode = toString(row[options.imageInitialColumn]);
        const finalNode = toString(row[options.finalColumn]);
        const classFinalNode = toString(row[options.classFinalColumn]);
        const imageFinalNode = toString(row[options.imageFinalColumn]);
        const relationship = toString(row[options.relationshipColumn]);

        if (nodes.length === 0) {
            nodes.push(startingNode);
            nodes.push(finalNode);
            elements.push({type: 'special', id: '0', data: { label: startingNode, class: classStartingNode, image: imageStartingNode}, position: { x: defaultXPosition, y: defaulYPosition } });
            elements.push({type: 'special', id:'1', data : { label: finalNode, class: classFinalNode , image: imageFinalNode }, position: { x: defaultXPosition, y: defaulYPosition }});
            elements.push({type: 'special', id:'e0-1', source: '0', target: '1',  label: relationship, arrowHeadType: 'arrowclosed' });
        } else {
            let indexNodeStart = 0;
            let indexNodeFinish = 0;
            if (!nodes.includes(startingNode)) {
                indexNodeStart = nodes.length
                nodes.push(startingNode);
                elements.push({type: 'special',  id: indexNodeStart.toString(), data: { label: startingNode, class: classStartingNode, image: imageStartingNode }, position: { x: defaultXPosition, y: defaulYPosition } });
            } else {
                indexNodeStart = nodes.indexOf(startingNode);
            }
            if (!nodes.includes(finalNode)) {
                indexNodeFinish = nodes.length
                nodes.push(finalNode);
                elements.push({type: 'special',  id: indexNodeFinish.toString(), data: { label: finalNode, class: classFinalNode, image: imageFinalNode }, position: { x: defaultXPosition, y: defaulYPosition } });
            } else {
                indexNodeFinish = nodes.indexOf(finalNode);
            }
            elements.push({id:'e'+indexNodeStart.toString()+'-'+indexNodeFinish.toString(),
                source: indexNodeStart.toString(), target: indexNodeFinish.toString(),  label: relationship, arrowHeadType: 'arrowclosed' });

        }
    });

    return getLayoutedElements(elements);
}

import "./renderer.less";

function scaleElement(node: any, container: any) {
  node.style.transform = null;
  const { width: nodeWidth, height: nodeHeight } = node.getBoundingClientRect();
  const { width: containerWidth, height: containerHeight } = container.getBoundingClientRect();

  const scaleX = containerWidth / nodeWidth;
  const scaleY = containerHeight / nodeHeight;

  node.style.transform = `scale(${Math.min(scaleX, scaleY)})`;
}

function render(container: any, words: any) {
  container = d3.select(container);
  container.selectAll("*").remove();

  const svg = container.append("svg");
  const g = svg.append("g");
  g.selectAll("text")
    .data(words)
    .enter()
    .append("text")
    .style("font-size", (d: any) => `${d.size}px`)
    .style("font-family", (d: any) => d.font)
    .style("fill", (d: any) => d.color)
    .attr("text-anchor", "middle")
    .attr("transform", (d: any) => `translate(${[d.x, d.y]}) rotate(${d.rotate})`)
    .text((d: any) => d.text);

  const svgBounds = svg.node().getBoundingClientRect();
  const gBounds = g.node().getBoundingClientRect();

  svg.attr("width", Math.ceil(gBounds.width)).attr("height", Math.ceil(gBounds.height));
  g.attr("transform", `translate(${svgBounds.left - gBounds.left},${svgBounds.top - gBounds.top})`);

  scaleElement(svg.node(), container.node());
}

const onLoad = (reactFlowInstance : any) => {
  console.log('flow loaded:', reactFlowInstance);
  reactFlowInstance.fitView();
};

const customNodeStyles = {
    background: '#FFF',
    color: '#000',
    padding: 10,
    border: '1px solid #000'
};


/* Clear floats after the columns*/
const row = { display: "grid", gridTemplateColumns: "repeat(2, 1fr)", gridGap: 2 }


// @ts-ignore
const CustomNodeComponent = ({ data }) => {
    // @ts-ignore
    let imageSource = "";
    let content: any = <div className = "centeredText">{data.label}<br/><p className="grayText leftText">{data.class}</p></div>;
    if (data.image.length != 0) {
        imageSource = "data:image/jpeg;base64," + data.image;
        content =
        <div style = {row}>
            <div><img className="photo" src={imageSource}/></div>
            <div className = "leftText">{data.label}<br/><p className="grayText leftText">{data.class}</p></div>
        </div>;
    }

    return (
        <div style={customNodeStyles}>
            <Handle type="target" position={Position.Left} style={{ borderRadius: 0 }} />
            {content}
            <Handle
                type="source"
                position={Position.Right}
                style={{ top: '30%', borderRadius: 0 }}
            />
        </div>
    );
};

export default function Renderer({ data, options }: any) {
    const [elements, setElements] = useState(getNodeGraph(data.rows, options));
    const onElementsRemove = (elementsToRemove : any) =>
        setElements((els : any) => removeElements(elementsToRemove, els));
    const onConnect = (params : any) => setElements((els : any) => addEdge(params, els));
    return (
        <ReactFlow
            elements={elements}
            nodeTypes={{ special: CustomNodeComponent }}
            onElementsRemove={onElementsRemove}
            onConnect={onConnect}
            onLoad={onLoad}
            snapToGrid={true}
            snapGrid={[15, 15]}
            style={{ height: 500 }}
            className={"graph-visualization-container"}
        >
          <MiniMap
              nodeStrokeColor = '#eee'
              nodeColor={(n : any ) => {
                if (n.style?.background) return n.style.background;
                return '#fff';
              }}
              nodeBorderRadius={2}
          />
          <Controls />
          <Background color="#aaa" gap={16} />
        </ReactFlow>
    );
}

Renderer.propTypes = RendererPropTypes;
